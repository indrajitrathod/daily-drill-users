const users = require('./userData');

//Find all users who are interested in video games.
const getUsersIntoVideoGames = (users) => {
    const result = Object.entries(users).filter(([user, userProfile]) => {
        const interest = users[user]["interests"].join("").toLowerCase();
        users[user]["interests"] = interest;
        return interest.includes("video games");
    }).map(([user, userProfile]) => {
        return { [user]: userProfile };
    });

    return result;
}

//Find all users staying in Germany.
const usersInGermany = (users) => {
    const result = Object.entries(users).filter(([user, userProfile]) => {
        return users[user]["nationality"].toLowerCase() === "germany";
    }).map(([user, userProfile]) => {
        return { [user]: userProfile };
    });

    return result;
}

//helper Function to assign seniority
const assignSeniority = (designation) => {
    designation = designation.toLowerCase();

    if (designation.includes("senior")) {
        return 0;
    } else if (designation.includes("developer")) {
        return 1;
    } else if (designation.includes("intern")) {
        return 2;
    }
}

// sorts in descending order of Designation and descending order of age
const sortBySeniority = (users) => {
    const result = Object.entries(users).sort((userA, userB) => {
        let seniorityA, seniorityB;

        seniorityA = assignSeniority(userA[1]["desgination"]);
        seniorityB = assignSeniority(userB[1]["desgination"]);

        if (seniorityA === seniorityB) {
            return userA[1]["age"] - userB[1]["age"];
        } else {
            return seniorityA - seniorityB;
        }
    }).map(([user, userProfile]) => {
        return { [user]: userProfile };
    });

    return result;
}

//  Find all users with masters Degree.
const getUsersWithMasters = (users) => {
    let result = Object.entries(users).filter(([user, userProfile]) => {
        return users[user]["qualification"].toLowerCase() === "masters";
    }).map(([user, userProfile]) => {
        return { [user]: userProfile };
    });
    return result;
}

// Group users based on their Programming language mentioned in their designation.
const groupByProgrammingLanguage = (users) => {
    let result = {};

    result = Object.keys(users).reduce((result, user) => {
        let language = users[user]["desgination"].replaceAll("-", "");
        language = language.split(" ");

        if (language.length === 3) {
            if (language[0] === 'Intern') {
                language = language[2];
            } else {
                language = language[1];
            }
        } else {
            language = language[0];
        }

        if (result.hasOwnProperty(language) === false) {
            result[language] = [];
        }

        result[language].push(user);

        return result;
    }, {});


    return result;
}


